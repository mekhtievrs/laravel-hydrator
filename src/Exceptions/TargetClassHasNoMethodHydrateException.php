<?php

namespace Mekhtievrs\Hydrator\Exceptions;

/**
 * Class TargetClassHasNoMethodHydrateException
 * @package Mekhtievrs\Hydrator\Exceptions
 */
class TargetClassHasNoMethodHydrateException extends \Exception
{

    /** @var string */
    protected $message = 'Target class has no hydrate method';

}