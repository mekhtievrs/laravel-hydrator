<?php

namespace Mekhtievrs\Hydrator;

use Illuminate\Support\ServiceProvider;

/**
 * Class HydratorServiceProvider
 * @package Mekhtievrs\Hydrator
 */
class HydratorServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([MakeMapperCommand::class]);
        }
    }

}