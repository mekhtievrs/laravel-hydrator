<?php

use Mekhtievrs\Hydrator\DataMapperInterface;

if (!function_exists('hydrator')) {
    function hydrator($data, DataMapperInterface $mapper = null)
    {
        return new Mekhtievrs\Hydrator\Hydrator($data, $mapper);
    }
}