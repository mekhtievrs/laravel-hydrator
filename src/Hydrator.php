<?php

namespace Mekhtievrs\Hydrator;

use Mekhtievrs\Hydrator\Exceptions\MapperNotSetException;
use Mekhtievrs\Hydrator\Exceptions\TargetClassHasNoMethodHydrateException;
use Illuminate\Support\Collection;

/**
 * Class Hydrator
 * @package App\Hydrators
 */
class Hydrator
{

    protected const STRATEGY_OBJECT = 'object';
    protected const STRATEGY_COLLECTION = 'collection';

    /** @var string */
    protected string $strategy;

    /** @var mixed */
    protected $data;

    /** @var string|null */
    protected ?string $targetModel = null;

    /** @var DataMapperInterface */
    protected DataMapperInterface $mapper;

    /**
     * Hydrator constructor.
     * @param array|object|Collection $data
     * @param DataMapperInterface|null $mapper
     */
    public function __construct($data, DataMapperInterface $mapper = null)
    {
        if (is_array($data)) {
            if ($this->isAssoc($data)) {
                $this->data = $data;
                $this->strategy = self::STRATEGY_OBJECT;
            } else {
                $this->data = collect($data);
                $this->strategy = self::STRATEGY_COLLECTION;
            }
        } elseif (is_object($data)) {
            if (is_a($data, Collection::class)) {
                $this->data = $data;
                $this->strategy = self::STRATEGY_COLLECTION;
            } else {
                $this->data = json_decode(json_encode($data), true);
                $this->strategy = self::STRATEGY_OBJECT;
            }
        }

        if ($mapper) {
            $this->with($mapper);
        }
    }

    /**
     * @param DataMapperInterface $mapper
     * @return $this
     */
    public function with(DataMapperInterface $mapper) : Hydrator
    {
        $this->mapper = $mapper;
        return $this;
    }

    /**
     * @param string $targetModel
     * @return $this
     */
    public function to(string $targetModel) : Hydrator
    {
        $this->targetModel = $targetModel;
        return $this;
    }

    /**
     * @return array|object|Collection
     * @throws \Throwable
     */
    public function hydrate()
    {
        throw_if(!$this->mapper, MapperNotSetException::class);
        return
            $this->strategy === self::STRATEGY_OBJECT
                ? $this->hydrateObject()
                : $this->hydrateCollection()
        ;
    }

    /**
     * @param array $array
     * @return bool
     */
    protected function isAssoc(array $array)
    {
        return array_values($array) !== $array;
    }

    /**
     * @return array
     * @throws \Throwable
     */
    protected function hydrateObject()
    {
        $result = $this->mapper->transform($this->data);
        if ($this->targetModel) {
            throw_if(!method_exists($this->targetModel, 'hydrate'), TargetClassHasNoMethodHydrateException::class);
            $result = ($this->targetModel)::hydrate([$result])->shift();
        }
        return $result;
    }

    /**
     * @return Collection
     * @throws \Throwable
     */
    protected function hydrateCollection() : Collection
    {
        $mappedCollection = $this->data->map(function ($item) {
            return $this->mapper->transform($item);
        });
        return
            $this->targetModel
                ? ($this->targetModel)::hydrate($mappedCollection->toArray())
                : $mappedCollection
            ;
    }

}
