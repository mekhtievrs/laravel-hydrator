<?php

namespace Mekhtievrs\Hydrator;

/**
 * Interface DataMapperInterface
 * @package App\Hydrators
 */
interface DataMapperInterface
{

    /**
     * @param array $data
     * @return array
     */
    public function transform(array $data) : array;

}
